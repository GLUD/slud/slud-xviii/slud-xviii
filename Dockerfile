FROM node:14 as node

WORKDIR /app

COPY package.json .

RUN yarn install

COPY . .

RUN yarn build -- --prod

FROM nginx

COPY --from=node /app/public/ /usr/share/nginx/html
COPY --from=node /app/nginx.conf /etc/nginx/conf.d/default.conf

CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/token.template.json > /usr/share/nginx/html/assets/token.json && exec nginx -g 'daemon off;'"]
