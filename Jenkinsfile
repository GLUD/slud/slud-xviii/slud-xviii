pipeline {
    agent any
    options {
      gitLabConnection('Gitlab')
      gitlabBuilds(builds: ['Code Test','Build Test', 'Build Image', 'Deploy Service'])
    }

    stages {
        stage('SonarQube Quality Test') {
            agent {
                label 'Jenkins'
            }
            environment {
                scannerHome = tool 'Sonarqube'
            }
            tools {
                nodejs 'Nodejs'
            }
            steps {
                updateGitlabCommitStatus name: 'Code Test', state: 'running'
                withSonarQubeEnv('Sonarqube') {
                    sh '${scannerHome}/bin/sonar-scanner -Dsonar.projectKey=slud'
                }
            }
            post {
                failure {
                    updateGitlabCommitStatus name: 'Code Test', state: 'failed'
                }
                aborted {
                    updateGitlabCommitStatus name: 'Code Test', state: 'canceled'
                }
            }
        }
        stage('SonarQube Quality Gate') {
            agent {
                label 'Jenkins'
            }
            steps {
                timeout(time: 1, unit: 'HOURS') {
                    waitForQualityGate abortPipeline: true, webhookSecretId: '908adf21-7da3-46df-a235-7b58b37ab6d3'
                }
            }
            post {
                failure {
                    updateGitlabCommitStatus name: 'Code Test', state: 'failed'
                }
                success {
                    updateGitlabCommitStatus name: 'Code Test', state: 'success'
                }
                aborted {
                    updateGitlabCommitStatus name: 'Code Test', state: 'canceled'
                }
            }
        }
        stage('Build Code') {
            agent {
                label 'Jenkins'
            }
            tools {
                nodejs 'Nodejs'
            }
            steps {
                updateGitlabCommitStatus name: 'Build Test', state: 'running'
                sh 'yarn install'
                sh 'yarn build -- --prod'
            }
            post {
                failure {
                    updateGitlabCommitStatus name: 'Build Test', state: 'failed'
                }
                success {
                    updateGitlabCommitStatus name: 'Build Test', state: 'success'
                }
                aborted {
                    updateGitlabCommitStatus name: 'Build Test', state: 'canceled'
                }
            }
        }
        stage('Docker Build') {
            agent {
                label 'Glud'
            }
            steps {
                updateGitlabCommitStatus name: 'Build Image', state: 'running'
                sh 'docker build -t glud/slud-front .'
                sh 'docker-compose -f docker-stack.yml config'
            }
            post {
                failure {
                    updateGitlabCommitStatus name: 'Build Image', state: 'failed'
                }
                success {
                    updateGitlabCommitStatus name: 'Build Image', state: 'success'
                    sh 'docker stack rm slud'
                }
                aborted {
                    updateGitlabCommitStatus name: 'Build Image', state: 'canceled'
                }
            }
        }
        stage('Docker Deploy') {
            agent {
                label 'Glud'
            }
            environment {
                ACCESS_TOKEN = credentials('d508bafe-4a9e-4e31-96f2-2d73503c3735')
                SECRET_KEY = credentials('64127ddf-1616-4515-b112-4764e1064d79')
                SITE_KEY = credentials('1269f7c5-d4ed-4754-b322-07765cffcc7d')
            }
            steps {
                updateGitlabCommitStatus name: 'Deploy Service', state: 'running'
                sh 'docker stack deploy -c docker-stack.yml slud'
            }
            post {
                failure {
                    updateGitlabCommitStatus name: 'Deploy Service', state: 'failed'
                }
                success {
                    updateGitlabCommitStatus name: 'Deploy Service', state: 'success'
                }
                aborted {
                    updateGitlabCommitStatus name: 'Deploy Service', state: 'canceled'
                }
            }
        }
    }
}
