<h1> SLUD XVIII - CIENCIA LIBRE - 2021</h1>

<p style="padding-bottom: 2rem;">
    <img widht="200px" height="200px" src="src/assets/img/Logo-02.svg">
    <br>
    <i> Página Web para la Semana Linux de la Universidad Distrital, 
    <br> en está oportunidad la abordará la temática de la ciencia libre y las tecnologías libres, 
    <br> el tema fue escogido gracias a la adición de nuevas carreras de pregrado.</i>
    <br>
</p>

<p>
  <a href="https://gitlab.com/GLUD">
    <img src="https://img.shields.io/badge/GLUD-gitlab-br?style=flat-square" alt="GLUD" />
  </a>&nbsp;
  <a href="https://www.gnu.org/licenses/gpl-3.0.html">
    <img src="https://img.shields.io/badge/License-GPL_V.3-blue?style=flat-square" alt="LICENSE" />
  </a>&nbsp;
</p>


<hr>
<br>

Generado con [Angular CLI](https://github.com/angular/angular-cli) version 11.2.2.

## Correr el server

Ejecutar `ng serve` para correr el server. Ir a `http://localhost:4200/`.

## Ayuda en el código

Ejecutar `ng generate component component-name` para generar un nuevo componente. Tambien puede usar `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Construyendo

Ejecutar `ng build` para construir el proyecto. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Ejecutando Tests Unitarios

Ejecute `ng test` para ejecutar test unitarios via [Karma](https://karma-runner.github.io).

## Ejecutando end-to-end tests

Ejecute `ng e2e` para ejecutar los end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

Para ayuda `ng help` o ir a [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## miniCurso de angular

[Clase - 9 de marzo del 2020](https://drive.google.com/file/d/12V5s7frO6iGsiabGp1H3MglJdu8NmDBC/view?usp=sharing)
