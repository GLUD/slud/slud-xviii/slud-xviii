# Assets folder

En esta carpeta se organizaran los recursos utilizados por la página Web

+----+ tipografias
     |
     + imagenes +
     |          |
     |          +-Sponsors
     |          |
     |          +-Conferencistas
     |              |
     |              +-Perfil
     |              |
     |              +-Empresa
     + etc

## Carpeta tiporafias 

Tipos de letra a utilizarce recordar que deben tener licencia libre o ser OpenFont

## Carpeta Imagenes

Imagenes requeridas por la página web

## Carpeta etc

Un espacio provisional para los assets sin categoria