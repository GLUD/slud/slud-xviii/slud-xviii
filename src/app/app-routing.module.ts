import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules } from '@angular/router';

import { EnConstruccionComponent } from './componentes/en-construccion/en-construccion.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { RegConferencitasComponent } from './componentes/reg-conferencitas/reg-conferencitas.component';
import { TiendaComponent } from './componentes/tienda/tienda.component';



const routes: Routes = [
  { path: '', component: InicioComponent },
  { path: 'store', component: TiendaComponent },
  {
    path: 'schedule',
    // component: ProgramacionComponent 
    loadChildren: () => import('./componentes/programacion/programacion.module').then(m => m.ProgramacionModule)
  },
  { path: 'store', component: EnConstruccionComponent },
  { path: 'register', component: RegConferencitasComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(
    routes,
    { preloadingStrategy: PreloadAllModules }
  )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
