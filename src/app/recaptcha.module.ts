import { NgModule } from '@angular/core';
import { RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module, RecaptchaFormsModule } from 'ng-recaptcha';
import { SingletonSettings } from './services/settings/settings.service';

export function getSiteKey() {
    return SingletonSettings.singletonSettings.settings.siteKey;
}

@NgModule({
  declarations: [],
  imports: [
    RecaptchaFormsModule,
    RecaptchaV3Module
  ],
  providers: [
    {
      provide: RECAPTCHA_V3_SITE_KEY,
      useFactory: getSiteKey
    }
  ]
})
export class RecaptchaModule { }
