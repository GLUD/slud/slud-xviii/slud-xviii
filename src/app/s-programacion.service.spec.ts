import { TestBed } from '@angular/core/testing';

import { SProgramacionService } from './s-programacion.service';

describe('SProgramacionService', () => {
  let service: SProgramacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SProgramacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
