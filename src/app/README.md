# Carpeta App

La carpeta más importante dentro de el proyecto aqui encontraras componentes y cada página web creada.

## Carpeta componentes 

Aquí vas a estar la mayoría del tiempo programando :) en esta carpeta podrás encontrar componentes la página web asignada, el contenido que vas a realizar.

## Archivos App.Component.*

Archivos de la aplicación principal, se distribuyen de igual forma que los componentes dentro de la apicacion con su hoja de estilos, su hoja de estructura (HTML), etc.
