import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  constructor(private http: HttpClient) { }
  public async initializeApp(): Promise<void> {
    const response = await this.http.get<Settings>('/assets/token.json').toPromise();
    SingletonSettings.init(response);
  }
}

export class SingletonSettings {
  public settings: Settings;

  private constructor(settings: Settings) {
    this.settings = settings;
  }
  public static singletonSettings: SingletonSettings;

  public static init(settings: Settings) {
    this.singletonSettings = new SingletonSettings(settings);
  }
}

interface Settings {
  token: string;
  siteKey: string;
  secretKey: string;
}