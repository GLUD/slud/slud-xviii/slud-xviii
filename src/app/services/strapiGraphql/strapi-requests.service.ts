import { Injectable } from '@angular/core';

import { MutationOptions, QueryOptions } from '@apollo/client/core';
import { Apollo, gql, WatchQueryOptions } from 'apollo-angular';
import { EmptyObject } from 'apollo-angular/types';
import { empty, EmptyError } from 'rxjs';

const queries = {
 
  ADD_SPEAKER: gql`
  mutation($conferencista: ConferencistaInput) {
    createConferencista(
      input: {
        data: $conferencista
      }
    ) {
      conferencista {
        id
      }
    }
  }
  `,
  GET_SPEAKER: gql`
  query($id: ID!) {
    conferencistaActivo(id: $id) {
      NombreCompleto
      NombreConferencia
      DescripcionConferencia
      FechaConferencia
      ProfileImage {
        url
      }
    }
  }
  `,
  GET_SCIENTISTS: gql`
  query {
    cientificos(
      sort: "nombreCientifico:asc"
    ) {
      imagenCientifico {
        url
      }
      nombreCientifico
    }
  } 
  `
  ,
  GET_SPEAKERS: gql`
  query {
    conferencistaActivos(
      sort: "FechaConferencia:asc"
    ) {
      NombreCompleto
      NombreConferencia
      DescripcionConferencia
      FechaConferencia
      ProfileImage {
        url
      }
    }
  }
  `,
  GET_SPONSORS: gql`
  query {
    patrocinadors {
      id
      Nombre
      Description
      Imagen {
        url
      }
    }
  }
  `,
  UPLOAD: gql`
  mutation($file: Upload!) {
    upload(file: $file) {
      id
    }
  }
  `,
  GET_STORE_ARTICLES: gql`
  query {
    storeArticles {
      id
      price
      description
      title
      stock
      image {
        url
      }
      images {
        id
        url
      }
    }
  }
  `
}

@Injectable({
  providedIn: 'root'
})
export class StrapiRequestsService {

  constructor(private apollo: Apollo) {}

  private watchQuery(options: WatchQueryOptions<EmptyObject, unknown>) {
    return this.apollo.watchQuery({
      ...options,
      pollInterval: 500
    });
  }

  private query(options: QueryOptions<EmptyObject, unknown>) {
    return this.apollo.query(options);
  }

  private mutation(options: MutationOptions<unknown, EmptyObject>) {
    return this.apollo.mutate(options);
  }

  upload(file) {
    return this.mutation({
      mutation: queries.UPLOAD,
      variables: {file},
      context: {
        useMultipart: true
      }
    });
  }



  async addConferencista(conferencista: { [x: string]: string | number; }) {
   
    let idImagen, idImagenPerfil;
    if(conferencista.logoOrganizacion) {
      idImagen = ((await this.upload(conferencista.logoOrganizacion).toPromise()).data as any).upload.id;
    }
    if(conferencista.imagenPerfil) {
      idImagenPerfil = ((await this.upload(conferencista.imagenPerfil).toPromise()).data as any).upload.id;
    }

    const fecha = new Date(conferencista.dia);
    fecha.setDate(fecha.getDate() + 1);
    fecha.setHours(+conferencista.hora);

    const conferencistaInput: { [x: string]: string | number; } = {
      NombreCompleto: conferencista.nombreCompleto,
      TipoIdentificacion: conferencista.tipoIdentificacion,
      NumeroIdentificacion: conferencista.numeroIdentificacion,
      Email: conferencista.email,
      Perfil: conferencista.perfilProfesional,
      RedSocial: conferencista.linkRedSocial,
      Celular: conferencista.celular.toString(),
      NombreConferencia: conferencista.nombreConferencia,
      DescripcionConferencia: conferencista.descripcionConferencia,
      FechaConferencia: fecha.toISOString(),
      Requerimientos: conferencista.requerimientos,
      Patrocinador: conferencista.patrocinador,
      Comentarios: conferencista.comentarios,
      logoOrganizacion: idImagen,
      imagenPerfil: idImagenPerfil
    }

    return this.mutation({
      mutation: queries.ADD_SPEAKER,
      variables: {"conferencista": conferencistaInput}
    });
  }

  getConferencista(id: string) {
    return this.query({
      query: queries.GET_SPEAKER,
      variables: {id}
    });
  }

  getConferencistas() {
    return this.watchQuery({
      query: queries.GET_SPEAKERS
    });
  }

  getPatrocinadores() {
    return this.watchQuery({
      query: queries.GET_SPONSORS
    });
  }

  getCientificos(){
    return this.watchQuery({
      query: queries.GET_SCIENTISTS
    });
  }

  getStoreArticles() {
    return this.watchQuery({
      query: queries.GET_STORE_ARTICLES
    });
  }

}
