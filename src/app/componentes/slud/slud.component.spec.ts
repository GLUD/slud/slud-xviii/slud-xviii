import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SludComponent } from './slud.component';

describe('SludComponent', () => {
  let component: SludComponent;
  let fixture: ComponentFixture<SludComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SludComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SludComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
