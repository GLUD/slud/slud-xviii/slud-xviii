import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-streams',
  templateUrl: './streams.component.html',
  styleUrls: ['./streams.component.css']
})
export class StreamsComponent implements OnInit {

  status = false;


  notify() {
    this.status = !this.status;
     
  }

  constructor() {}

  ngOnInit(): void {
  }

}
