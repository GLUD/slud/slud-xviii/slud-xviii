import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StreamsNotifyComponent } from './streams-notify.component';

describe('StreamsNotifyComponent', () => {
  let component: StreamsNotifyComponent;
  let fixture: ComponentFixture<StreamsNotifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StreamsNotifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StreamsNotifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
