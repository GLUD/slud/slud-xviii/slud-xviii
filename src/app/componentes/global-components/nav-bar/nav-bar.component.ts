import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor() { }
  

  icono_menu:String = "fab fa-linux";

  activeMenu(): void {
      if(window.innerWidth <= 858){
        const menuItem = document.querySelector('.menu-items');
        menuItem?.classList.toggle('show');
        const btn_ico = document.getElementById('btn-icon');

        if(menuItem?.className.includes("show")){
          this.icono_menu = "fas fa-times"
        }else{
          this.icono_menu = "fab fa-linux"
        }
      }
      
        
  }

  ngOnInit(): void {
    
  }

}
