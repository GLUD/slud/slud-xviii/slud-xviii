import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  estilo_footer: String = "footer";

  constructor() { }

  ngOnInit(): void {
  }

  abrir_footer(){
    
    const el_footer = document.getElementById("footer");

    if(el_footer?.className.includes("subir")){
      this.estilo_footer = "footer"
      
    }else{
      this.estilo_footer = "footer subir"
    }

  }

}
