export interface ItimerItem {
    days: number,
    hours:number,
    minutes:number,
    seconds:number
}
