import { Component, OnInit , Input} from '@angular/core';
import * as countdown from 'countdown';
import { ItimerItem } from './Itimer-item.metadata';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
  @Input() date:Date = new Date();
  
  time: ItimerItem = null;
  timerId: number = null;
  

  constructor() { }

  ngOnInit(): void {
    this.timerId = countdown(this.date, (ts) => {
      this.time = ts;
    }, countdown.DAYS | countdown.HOURS | countdown.MINUTES | countdown.SECONDS );
  }

  ngOnDestroy(): void {
    if(this.timerId){
      clearInterval(this.timerId);
    }
  }

}
