export class IcarruselItem {
    constructor({id, url}) {
        this.id = id;
        this.image = url;
    }
    id: string | number;
    title?:{
        first: string;
        second: string;
    };
    subtitle?: string;
    link?: string;
    image: string;
    order?: number;
    marginLeft?: number;

}