import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgImgComponent } from './prog-img.component';

describe('ProgImgComponent', () => {
  let component: ProgImgComponent;
  let fixture: ComponentFixture<ProgImgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProgImgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
