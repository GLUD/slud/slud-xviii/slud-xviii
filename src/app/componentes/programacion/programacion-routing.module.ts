import { Route } from '@angular/compiler/src/core';
import { NgModule } from '@angular/core' ;
import { RouterModule, Routes } from '@angular/router';

import { ProgramacionComponent } from './components/programacion/programacion.component';

const routes: Routes =[
    {
        path:'',
        component: ProgramacionComponent,
    }
]
 
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ProgramacionRoutingModule {}