import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgImgComponent } from './components/prog-img/prog-img.component';
import { ProgramacionComponent } from './components/programacion/programacion.component';

import { ProgramacionRoutingModule } from './programacion-routing.module';

@NgModule({
    declarations:[
        ProgImgComponent,
        ProgramacionComponent
    ],
    imports:[
        CommonModule,
        ProgramacionRoutingModule
    ]
})

export class ProgramacionModule{}