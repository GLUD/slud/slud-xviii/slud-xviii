export class Conferencista {
    Imagen: any;
    NombreConferencia: any;
    DescripcionConferencia: any;
    FechaConferencia: any;
    NombreCompleto: any;

    constructor(object) {
        this.Imagen = object.imagen;
        this.NombreConferencia = object.NombreConferencia;
        this.DescripcionConferencia = object.DescripcionConferencia;
        this.FechaConferencia = object.FechaConferencia;
        this.NombreCompleto = object.NombreCompleto;
    }

}