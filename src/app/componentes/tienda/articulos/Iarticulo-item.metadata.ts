import { IcarruselItem } from '../../carrusel/Icarrusel-item.metadata';

export class IarticuloItem {
  constructor({id, price, description, title, stock, image}, images) {
    this.id = id;
    this.price = price;
    this.description = description;
    this.title = title;
    this.stock = stock;
    this.image = image
    this.images = images;
  };
  id: string | number;
  price: number;
  description: string;
  title: string;
  stock: number;
  image: any;
  images: IcarruselItem[];
}
