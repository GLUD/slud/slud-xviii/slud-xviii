import { Component, OnInit, ViewChild } from '@angular/core';
import { NgImageSliderComponent } from 'ng-image-slider';
import { CARRUSEL_DATA_ITEMS } from '../carrusel/carrusel.const';
import { IcarruselItem } from '../carrusel/Icarrusel-item.metadata';

@Component({
  selector: 'app-tienda',
  templateUrl: './tienda.component.html',
  styleUrls: ['./tienda.component.css']
})
export class TiendaComponent implements OnInit {

  public carruselData: IcarruselItem[] = CARRUSEL_DATA_ITEMS;

  constructor() { }

  ngOnInit(): void {
  }

  @ViewChild('nav') slider: NgImageSliderComponent;

  imageObject: Array<object> = [
    {
    image: 'assets/img/camiseta/front.png',
    thumbImage: 'assets/img/camiseta/front.png',
    alt: 'alt of image',
    title: 'Frente'
    }, 
    {
      image: 'assets/img/camiseta/left.png',
      thumbImage: 'assets/img/camiseta/left.png',
      alt: 'alt of image',
      title: 'Lateral Izquierdo'
    },
    {
      image: 'assets/img/camiseta/back.png',
      thumbImage: 'assets/img/camiseta/back.png',
      alt: 'alt of image',
      title: 'Espalda'
    },
    {
      image: 'assets/img/camiseta/right.png',
      thumbImage: 'assets/img/camiseta/right.png',
      alt: 'alt of image',
      title: 'Lateral Derecho'
    }    
  ];

  prevImageClick() {
    this.slider.prev();
  }

  nextImageClick() {
    this.slider.next();
  }
}
