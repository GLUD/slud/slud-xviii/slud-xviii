import { Component, OnInit , EventEmitter, Output, Input} from '@angular/core';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.css']
})
export class ArticuloComponent implements OnInit {

  @Output() cambio = new EventEmitter();
  @Input() id = 0;
  @Input() price = 0;
  @Input() image = 'https://cdn.pixabay.com/photo/2014/05/02/21/50/laptop-336378_960_720.jpg';
  @Input() description = 'Not found';


  constructor() {
  }

  ngOnInit(): void {
  }

  click(){
    this.cambio.emit(this.id);
  }

  toCop(value) {
    const formatter = new Intl.NumberFormat('es-CO', {
        style: 'currency',
        currency: 'COP'
    })

    return formatter.format(value)
  }

}
