import { Component, Input, OnInit , ViewChild} from '@angular/core';
import { CarruselComponent } from '../../carrusel/carrusel.component';
import { IcarruselItem } from '../../carrusel/Icarrusel-item.metadata';
import { IarticuloItem } from '../articulos/Iarticulo-item.metadata';

@Component({
  selector: 'app-modalarticulo',
  templateUrl: './modalarticulo.component.html',
  styleUrls: ['./modalarticulo.component.css'],
})
export class ModalarticuloComponent implements OnInit {

  @ViewChild(CarruselComponent) carrusel: CarruselComponent;

  public description:IarticuloItem;
  public carruselData: IcarruselItem[] = [];
  public show = true;

  constructor() {}

  ngOnInit(): void {}

  showModal() {
    this.show = true;
  }

  hideModal() {
    this.show = false;
    
  }

  setDescription(description:IarticuloItem){
    this.description = description;
  }

  setcarruselData(carruselData: IcarruselItem[]){
    this.carruselData = carruselData;
    // this.carrusel.update();
  }
}
