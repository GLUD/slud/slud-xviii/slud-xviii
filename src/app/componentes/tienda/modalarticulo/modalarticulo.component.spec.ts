import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalarticuloComponent } from './modalarticulo.component';

describe('ModalarticuloComponent', () => {
  let component: ModalarticuloComponent;
  let fixture: ComponentFixture<ModalarticuloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalarticuloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalarticuloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
