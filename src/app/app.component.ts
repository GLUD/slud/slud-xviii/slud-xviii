import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { NotifyService } from './services/streams/notify.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title: string;
  subscription: Subscription;
  status: boolean;
  streamUrl: string;

  constructor(private streamsService: NotifyService) {
    this.title = 'SLUD-XIII';
    this.status = false;
    this.streamUrl = '';
  }

  ngOnInit() {
    this.subscription = this.streamsService.getStreamStatus().valueChanges.subscribe(({data}: any) => {
      this.status = data.stream.isStreaming;
      this.streamUrl = data.stream.streamUrl;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}